#Thema
Bei der zu entwickelnden App geht es darum, eine Bediensoftware für eine physikalische Wanduhr (Vertical Clock) zu erstellen.
Die App ist nur im Zusammenhang mit einer vorhandenen Wanduhr sinnvoll einzusetzen.

#Umfeld, Ausgangslage
Bei der Wanduhr Vertical Clock handelt es sich in erster Linie um eine Zeitanzeige, die die aktuelle Uhrzeit mit einer Auflösung von einer Minute anzeigt und autonom läuft. Mit Hilfe einer App sollen zukünftig weitere Informationen wie z.B. Datum, Sportresultate usw. angezeigt werden.
Für die erstmalige Einrichtung und für die Benutzerinteraktion während dem Betrieb braucht es ein Userinterface. Vertical Clock verfügt über keine physikalischen Tasten, um dennoch Benutzereingaben zu ermöglichen, wurde ein Webserver integriert. Dieser lässt sich ins lokale Netzwerk einbinden und ist dadurch mit jedem Standard Webbrowser erreichbar. Bislang geschieht die Benutzerinteraktion über ein Webinterface, dass Anfragen über „einfache“ http Befehle anfordert, der integrierte Webserver liefert daraufhin die erforderlichen Daten zurück.