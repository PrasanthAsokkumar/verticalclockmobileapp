package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCSensorResult(@SerializedName("value_9")
                          var tempSensorActTemperature : Int = 0,
                          @SerializedName("value_10")
                          var lightSensorActBrightness : Int = 0,
                          @SerializedName("value_11")
                          var lightSensorOnOff : Int = 0,
                          @SerializedName("value_12")
                          var lightSensorThreshold : Int = 0,
                          @SerializedName("value_13")
                          var motionSensorActMotionStatus : Int = 0,
                          @SerializedName("value_14")
                          var motionSensorOnOff : Int = 0,
                          @SerializedName("value_15")
                          var motionSensorOffDelay : Int = 0,
                          @SerializedName("value_16")
                          var soundSensorActLevel : Int = 0,
                          @SerializedName("value_17")
                          var soundSensorOnOff : Int = 0,
                          @SerializedName("value_18")
                          var soundSensorOffDelay : Int = 0,
                          @SerializedName("value_19")
                          var soundSensorThreshold : Int = 0,
                          @SerializedName("value_20")
                          var soundSensorThresholdClap : Int = 0)
{
}