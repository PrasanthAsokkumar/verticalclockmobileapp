package com.verticalclock.verticalclockapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Toast
import com.verticalclock.verticalclockapp.MainApplication
import com.verticalclock.verticalclockapp.R
import com.verticalclock.verticalclockapp.R.id.*
import com.verticalclock.verticalclockapp.helpers.*
import com.verticalclock.verticalclockapp.models.api.VCStatisticsResult
import com.verticalclock.verticalclockapp.models.api.VCTriggerCounterResetResult
import kotlinx.android.synthetic.main.statistics_fragment.*
import java.util.*

/**
 * Created by Klaus Gerber on 18.02.2018.
 * Logic for the elements on the statistics-page
 */
class StatisticsFragment : Fragment() {
    private var refreshDataPending = false
    private var resetStatisticsCounterPendeing = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
        menu?.findItem(R.id.action_refresh)?.setVisible(false)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.statistics_fragment, null)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        // Request to VerticalClock --> Refresh Data
        refreshDataPending = true
        tryRefreshData()

        // Swipe-Down Updater
        refreshStatistics.setOnRefreshListener {
            // Request to VerticalClock --> Refresh Data
            refreshDataPending = true
            tryRefreshData()
        }

        btnResetStatistics.setOnClickListener {
            if (!SettingsHelper().isSimulationMode()) {
                // Request to VerticalClock --> Reset statistics counter
                resetStatisticsCounterPendeing = true
                tryRefreshData()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        refreshStatistics?.setRefreshing(false)
    }

    // Send a request to VeticalClock if the network settings are ok, and no other request ist pending
    fun tryRefreshData() {

        if (ConnectivityHelper().isWifiConnected() || SettingsHelper().isSimulationMode()) {
            // Busy, networkrequest not possible
            if (APIConnectorImpl.busy) {
                Log.d(getString(R.string.time_fragment), getString(R.string.new_request_try))
                refreshStatistics?.isRefreshing = true
                val snackbar = Snackbar
                        .make(refreshStatistics, getString(R.string.networkrequest_not_possible), 10000)
                        .setAction(getString(R.string.retry)) {
                            tryRefreshData()
                        }
                snackbar.setActionTextColor(Color.RED)
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (!APIConnectorImpl.busy) {
                            refreshStatistics?.setRefreshing(false)
                        }
                    }
                })
                snackbar.show()
            } else {
                // Send the request according the preselection
                if (refreshDataPending) {
                    refreshData()
                } else if (resetStatisticsCounterPendeing) {
                    resetStatisticsCounter()
                }
            }
        } else {
            // Networkstatus not OK, inform user
            val snackbar = Snackbar
                    .make(refreshStatistics, getString(R.string.networkstatus_nok), 10000)
                    .setAction(getString(R.string.ok)) {
                        val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                        startActivity(intent)
                    }
            snackbar.setActionTextColor(Color.RED)
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    super.onDismissed(snackbar, event)
                    if (!APIConnectorImpl.busy) {
                        refreshStatistics?.setRefreshing(false)
                    }
                }
            })
            snackbar.show()
        }
    }


    // Makes the networkrequest if the pre-check was OK --> Refresh data
    fun refreshData() {
        refreshStatistics?.isRefreshing = true
        if (SettingsHelper().isSimulationMode()) {
            val random = Random()
            tvOperatingHours?.text = random.nextInt(2000).toString()
            tvDistanceLL?.text = random.nextInt(150000).toString()
            tvDistanceLM?.text = random.nextInt(50000).toString()
            tvDistanceMR?.text = random.nextInt(150000).toString()
            tvDistanceRR?.text = random.nextInt(50000).toString()
            tvTriggerCountMotionSensor?.text = random.nextInt(500).toString()
            tvTriggerCountSoundSensor?.text = random.nextInt(500).toString()
            tvTriggerCountLightSensor?.text = random.nextInt(500).toString()
            tvTriggerCountUserRequests?.text = random.nextInt(500).toString()
            refreshStatistics?.setRefreshing(false)
            refreshDataPending = false

        } else {
            APIConnectorImpl.busy = true
            val call = APIConnectorImpl.api.getStatistics(ApiActions.GetStatistics.actionCode)
            call.enqueue(ApiCallback<VCStatisticsResult>({
                APIConnectorImpl.busy = false
                if (it != null) {
                    tvOperatingHours?.text = "${it.operatingHours}"
                    tvDistanceLL?.text = "${it.distanceMotorLL}"
                    tvDistanceLM?.text = "${it.distanceMotorLM}"
                    tvDistanceMR?.text = "${it.distanceMotorMR}"
                    tvDistanceRR?.text = "${it.distanceMotorRR}"
                    tvTriggerCountMotionSensor?.text = "${it.motionSensorTriggerCounts}"
                    tvTriggerCountSoundSensor?.text = "${it.soundSensorTriggerCounts}"
                    tvTriggerCountLightSensor?.text = "${it.lightSensorTriggerCounts}"
                    tvTriggerCountUserRequests?.text = "${it.userRequestTriggerCounts}"
                } else {
                    connectionError()
                }
                refreshStatistics?.setRefreshing(false)
                refreshDataPending = false
            }))
        }
    }


    // Makes the networkrequest if the pre-check was OK --> Reset the statistics trigger counter
    fun resetStatisticsCounter() {
        refreshStatistics?.isRefreshing = true
        APIConnectorImpl.busy = true
        val call = APIConnectorImpl.api.setTriggerCounterToZero(ApiActions.SetTriggerCounterToZero.actionCode)
        call.enqueue(ApiCallback<VCTriggerCounterResetResult>({
            APIConnectorImpl.busy = false
            if (it != null) {
                refreshDataPending = true
                tryRefreshData()
            } else {
                refreshStatistics?.setRefreshing(false)
                connectionError()
            }
            resetStatisticsCounterPendeing = false
        }))
    }

    // Show the user a Toast message if a networkrequest was not successful
    fun connectionError() {
        if (this.isVisible) {
            val toast = Toast.makeText(MainApplication.applicationContext(), getString(R.string.verticalclock_not_available), Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}