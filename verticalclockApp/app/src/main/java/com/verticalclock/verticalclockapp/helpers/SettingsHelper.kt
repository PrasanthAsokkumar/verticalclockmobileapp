package com.verticalclock.verticalclockapp.helpers

import android.app.Application
import com.verticalclock.verticalclockapp.MainActivity
import com.verticalclock.verticalclockapp.MainApplication

/**
 * Created by prasanthasokkumar on 05.02.18.
 */

class SettingsHelper
{
    private val API_BASE_URL = "http://192.168.4.1"
    private var preferenceHelper: PreferenceHelper

    init {
        preferenceHelper = PreferenceHelper()
    }

    /**
     * retrieve if App is init.
     */
    fun isAppInitialized() : Boolean
    {
        return preferenceHelper.getBooleanPreference(MainApplication.applicationContext(), preferenceHelper.KEY_USEING_FIRSTTIME, false)
    }

    fun setAppInitialized() : Boolean
    {
        return preferenceHelper.setBooleanPreference(MainApplication.applicationContext(), preferenceHelper.KEY_USEING_FIRSTTIME, true)
    }

    fun setIPAddress(ipaddress: String) : Unit
    {
        var url: String = "http://" + ipaddress
        preferenceHelper.setStringPreference(MainApplication.applicationContext(), preferenceHelper.KEY_VERTICALCLOCK_IPADDRESS, url)
    }

    fun getIPAddress() : String?
    {
        if(this.isAppInitialized())
        {
            return preferenceHelper.getStringPreference(MainApplication.applicationContext(), preferenceHelper.KEY_VERTICALCLOCK_IPADDRESS)
        }else
        {
            return API_BASE_URL
        }
    }

    fun setSimulationMode(state : Boolean){
        preferenceHelper.setBooleanPreference(MainApplication.applicationContext(), preferenceHelper.KEY_VERTICALCLOCK_SIMULATIONMODE, state)

    }

    fun isSimulationMode() : Boolean{
        return preferenceHelper.getBooleanPreference(MainApplication.applicationContext(), preferenceHelper.KEY_VERTICALCLOCK_SIMULATIONMODE, false)
    }
}