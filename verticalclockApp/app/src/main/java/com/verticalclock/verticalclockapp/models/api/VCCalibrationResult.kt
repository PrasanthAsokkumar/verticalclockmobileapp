package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest  //ToDO auf english alle stellen kontrollieren
 */

data class VCCalibrationResult(@SerializedName("value_0")
                         var CalibrationOk : Int = 0)
{
}