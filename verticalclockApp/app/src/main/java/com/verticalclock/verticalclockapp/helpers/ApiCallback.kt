package com.verticalclock.verticalclockapp.helpers

import android.util.Log
import android.widget.Toast
import com.verticalclock.verticalclockapp.MainApplication
import com.verticalclock.verticalclockapp.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Klaus Gerber on 26.02.2018.
 * Callback auf eine Netzwerkanfrage an Vertical Clock
 */
class ApiCallback<T>(val callback: ((response: T?) -> Unit)): Callback<T> {
    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            Log.d(MainApplication.applicationContext().resources.getString(R.string.api_callback), response.body()?.toString())
            callback(response.body())
        } else {
            Log.e(MainApplication.applicationContext().resources.getString(R.string.api_callback), MainApplication.applicationContext().resources.getString(R.string.error_api_call))
            callback(null)
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        Log.d(MainApplication.applicationContext().resources.getString(R.string.error), t.message)
        callback(null)
    }
}