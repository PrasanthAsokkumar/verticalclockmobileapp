package com.verticalclock.verticalclockapp


import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Toast
import com.verticalclock.verticalclockapp.helpers.*
import com.verticalclock.verticalclockapp.models.api.VCModeGetResult
import com.verticalclock.verticalclockapp.models.api.VCModeSetResult
import com.verticalclock.verticalclockapp.models.api.VCSensorResult
import kotlinx.android.synthetic.main.temperature_fragment.*
import java.util.*


/**
 * Created by Klaus Gerber on 18.02.2018.
 * Logic for the elements on the temperature-page
 */
class TemperatureFragment : Fragment() {
    private var drawable: Drawable? = null
    private var refreshDataPending = false
    private var firstAnimationOver = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
        drawable = menu?.findItem(R.id.action_refresh)?.icon
        drawable?.mutate()

        // Request to VerticalClock --> Refresh Data
        firstAnimationOver = false
        refreshDataPending = true
        tryRefreshData()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.temperature_fragment, null)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Swipe-Down Updater
        refreshTemperature.setOnRefreshListener {
            // Request to VerticalClock --> Refresh Data
            refreshDataPending = true
            tryRefreshData()
        }
    }

    override fun onPause() {
        super.onPause()
        refreshTemperature?.setRefreshing(false)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_refresh) {

            // Request to VerticalClock --> Set the temperature mode
            if (!APIConnectorImpl.busy) {
                drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)

                if (!SettingsHelper().isSimulationMode()) {
                    refreshTemperature?.isRefreshing = true
                    APIConnectorImpl.busy = true
                    val call = APIConnectorImpl.api.setMode(ApiActions.SetModeTemperature.actionCode)
                    call.enqueue(ApiCallback<VCModeSetResult>({
                        APIConnectorImpl.busy = false
                        if (refreshTemperature != null) {
                            refreshTemperature?.setRefreshing(false)
                        } else {
                            connectionError()
                        }
                    }))
                }
            }
        }
        return false
    }


    // Send a request to VeticalClock if the network settings are ok, and no other request ist pending
    fun tryRefreshData() {

        if (ConnectivityHelper().isWifiConnected() || SettingsHelper().isSimulationMode()) {
            // Busy, networkrequest not possible
            if (APIConnectorImpl.busy) {
                Log.d(getString(R.string.temperature_fragment), getString(R.string.new_request_try))
                refreshTemperature?.isRefreshing = true
                val snackbar = Snackbar
                        .make(refreshTemperature, getString(R.string.networkrequest_not_possible), 10000)
                        .setAction(getString(R.string.retry)) {
                            tryRefreshData()
                        }
                snackbar.setActionTextColor(Color.RED)
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        // if (refreshTemperature != null) {
                        if (!APIConnectorImpl.busy) {
                            refreshTemperature?.setRefreshing(false)
                        }
                        // }
                    }
                })
                snackbar.show()
            } else {
                // Send the request according the preselection
                if (refreshDataPending) {
                    refreshData()
                }
            }
        } else {
            // Networkstatus not OK, inform user
            val snackbar = Snackbar
                    .make(refreshTemperature, getString(R.string.networkstatus_nok), 10000)
                    .setAction(getString(R.string.ok)) {
                        val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                        startActivity(intent)
                    }
            snackbar.setActionTextColor(Color.RED)
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    super.onDismissed(snackbar, event)
                    if (!APIConnectorImpl.busy) {
                        refreshTemperature?.setRefreshing(false)
                    }
                }
            })
            snackbar.show()
        }
    }


    // Makes the networkrequest if the pre-check was OK --> Refresh data
    fun refreshData() {
        refreshTemperature?.isRefreshing = true

        if (SettingsHelper().isSimulationMode()) {
            val random = Random()
            val temp = String.format("%02d", random.nextInt(39))
            temperatureView?.setTemperature(temp)
            refreshTemperature?.setRefreshing(false)
            refreshDataPending = false
        } else {
            APIConnectorImpl.busy = true
            val callPartOne = APIConnectorImpl.api.getActualSensorValues(ApiActions.GetActualSensorValues.actionCode)
            callPartOne.enqueue(ApiCallback<VCSensorResult>({
                APIConnectorImpl.busy = false
                if (it != null) {
                    if (temperatureView != null) {

                        if (!firstAnimationOver) {
                            temperatureView.setTemperature(String.format("%02d", Integer.min(Integer.max(it.tempSensorActTemperature, 0), 39)))
                        } else {
                            temperatureView.setTemperatureWithoutAnimation(it.tempSensorActTemperature.toString())
                        }
                        firstAnimationOver = true

                        refreshTemperature?.isRefreshing = true
                        val callPartTwo = APIConnectorImpl.api.getMode(ApiActions.GetMode.actionCode)
                        callPartTwo.enqueue(ApiCallback<VCModeGetResult>({
                            APIConnectorImpl.busy = false
                            if (it != null) {
                                val actualMode = it.mode
                                if (actualMode != ApiActions.TemperatureMode.actionCode) {
                                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                                }
                            } else {
                                connectionError()
                            }
                            refreshTemperature?.setRefreshing(false)
                        }))
                    }

                } else {
                    connectionError()
                }
                refreshTemperature?.setRefreshing(false)
                refreshDataPending = false
            }))
        }
    }

    // Show the user a Toast message if a networkrequest was not successful
    fun connectionError() {
        if (this.isVisible) {
            val toast = Toast.makeText(MainApplication.applicationContext(), getString(R.string.verticalclock_not_available), Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}