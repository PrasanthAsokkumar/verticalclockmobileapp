package com.verticalclock.verticalclockapp

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Parcel
import android.os.Parcelable
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.view.PagerAdapter
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.verticalclock.verticalclockapp.helpers.SettingsHelper
import java.util.regex.Matcher
import java.util.regex.Pattern
import java.util.zip.Inflater


/**
 * Created by prasanthasokkumar on 17.03.18.
 */
class OnboardingPagerAdapter : PagerAdapter()
{
    private var settingsHelper: SettingsHelper = SettingsHelper()

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return  view === `object`
    }
    override fun getCount(): Int {
        return  3
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {

        if (position == 0)
        {
            val layout = LayoutInflater.from(MainApplication.applicationContext())?.inflate(R.layout.onboarding_view_one, container, false) as ViewGroup
            container?.addView(layout)
            return layout
        }else if(position == 1)
        {
            val layout = LayoutInflater.from(MainApplication.applicationContext())?.inflate(R.layout.onboarding_view_two, container, false) as ViewGroup
            container?.addView(layout)
            return layout
        }else
        {
            val layout = LayoutInflater.from(MainApplication.applicationContext())?.inflate(R.layout.onboarding_view_three, container, false) as ViewGroup
            var etxtIpaddress = layout.findViewById<EditText>(R.id.etIPAdress)

            etxtIpaddress.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    var btnLosLegen = layout.findViewById<Button>(R.id.btnLosLegen)
                    if(etxtIpaddress?.text.toString().length > 7)
                    {
                        btnLosLegen.isEnabled = true
                        btnLosLegen.setTextColor(Color.GREEN)
                    }else
                    {
                        btnLosLegen.setTextColor(Color.BLACK)
                        btnLosLegen.isEnabled = false
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            })

            var btnLosLegen = layout.findViewById<Button>(R.id.btnLosLegen)
            btnLosLegen.setOnClickListener{
                settingsHelper.setIPAddress(etxtIpaddress.text.toString())
                settingsHelper.setAppInitialized()

                    val mainactivity = Intent(MainApplication.applicationContext(), MainActivity::class.java)
                    startActivity(MainApplication.applicationContext(), mainactivity, null)
            }

            container?.addView(layout)
            return layout
        }
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(`object` as View)
    }
}