package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCStatisticsResult(@SerializedName("value_1")
                             var operatingHours : Int = 0,
                             @SerializedName("value_2")
                             var startupCounts : Int = 0,
                             @SerializedName("value_6")
                             var motionSensorTriggerCounts : Int = 0,
                             @SerializedName("value_7")
                             var soundSensorTriggerCounts : Int = 0,
                             @SerializedName("value_8")
                             var lightSensorTriggerCounts : Int = 0,
                             @SerializedName("value_12")
                             var distanceMotorLL : Int = 0,
                             @SerializedName("value_13")
                             var distanceMotorLM : Int = 0,
                             @SerializedName("value_14")
                             var distanceMotorMR : Int = 0,
                             @SerializedName("value_15")
                             var distanceMotorRR : Int = 0,
                             @SerializedName("value_17")
                             var userRequestTriggerCounts : Int = 0)
{

}