package com.verticalclock.verticalclockapp.helpers

import com.verticalclock.verticalclockapp.models.api.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Hier werden die Verschiedenen Netzwerk Requests zu VerticalClock definiert
 */
const val NETWORK_ACTION = "ACTION"
const val NETWORK_VAL_1 = "Value1"
const val NETWORK_VAL_2 = "Value2"
const val NETWORK_VAL_3 = "Value3"
const val NETWORK_VAL_4 = "Value4"

interface APIConnector {
    // Gibt die aktuelle Zeit und das Datum von VerticalClock zurück
    @GET("/")
    fun getActualDateAndTime(@Query(NETWORK_ACTION) action: Int): Call<VCDateTimeResult>

    // Gibt den aktuellen Timerwert von VerticalClock zurück
    @GET("/")
    fun getTimer(@Query(NETWORK_ACTION) action: Int): Call<VCTimerGetResult>

    // Gibt die aktuellen Werte der Sensoren von VerticalClock zurück
    @GET("/")
    fun getActualSensorValues(@Query(NETWORK_ACTION) action: Int): Call<VCSensorResult>

    // Gibt die aktuellen Statistik Daten von VerticalClock zurück
    @GET("/")
    fun getStatistics(@Query(NETWORK_ACTION) action: Int): Call<VCStatisticsResult>

    // Gibt aktuelle Einstellungen von VerticalClock zurück
    @GET("/")
    fun getSettings(@Query(NETWORK_ACTION) action: Int): Call<VCSettingsResult>

    // Gibt die aktuellen Referenzierungseinstellungen von VerticalClock zurück
    @GET("/")
    fun getReferencingParameter(@Query(NETWORK_ACTION) action: Int): Call<VCGetReferencingParameterResult>

    // Setzt die Einstellungen für den Bewegungsmelder auf VerticalClock
    @GET("/")
    fun setMotionSoundParameter(@Query(NETWORK_VAL_1) onOffSwitch: String,
                                @Query(NETWORK_VAL_2) offDelayMin: String,
                                @Query(NETWORK_ACTION) action: Int): Call<VCMotionSensorResult>

    // Setzt die Einstellungen für den Geräuschsensor auf VerticalClock
    @GET("/")
    fun setSoundSensorParameter(@Query(NETWORK_VAL_1) onOffSwitch: String,
                                @Query(NETWORK_VAL_2) offDelayMin: String,
                                @Query(NETWORK_VAL_3) onThreshold: String,
                                @Query(NETWORK_VAL_4) onThresholdClap: String,
                                @Query(NETWORK_ACTION) action: Int): Call<VCSoundSensorResult>

    // Setzt die Einstellungen für den Helligkeitssensor auf VerticalClock
    @GET("/")
    fun setLightSensorParameter(@Query(NETWORK_VAL_1) onOffSwitch: String,
                                @Query(NETWORK_VAL_2) onThreshold: String,
                                @Query(NETWORK_ACTION) action: Int): Call<VCLightSensorResult>

    // Setzt die Einstellungen für die automatische Referenzierung nach x-Tagen auf VerticalClock
    @GET("/")
    fun setReferencingParameter(@Query(NETWORK_VAL_1) afterDays: String,
                                @Query(NETWORK_ACTION) action: Int): Call<VCSetReferencingParamResult>

    // Setzt den Modus auf VerticalClock (Zeit, Temperatur, Timer...)
    @GET("/")
    fun setMode(@Query(NETWORK_ACTION) action: Int): Call<VCModeSetResult>

    // Gibt den aktiven Modus von VerticalClock zurück (Zeit, Temperatur, Timer...)
    @GET("/")
    fun getMode(@Query(NETWORK_ACTION) action: Int): Call<VCModeGetResult>

    // Übergibt einen Timerwert und setzt VerticalClock in den Timer-Modus
    @GET("/")
    fun setTimer(@Query(NETWORK_VAL_1) hh_mm: String,
                 @Query(NETWORK_ACTION) action: Int): Call<VCTimerSetResult>

    // Löst eine Kalibrierung (Referenzierung) auf VerticalClock aus
    @GET("/")
    fun setCalibration(@Query(NETWORK_ACTION) action: Int): Call<VCCalibrationResult>

    // Setzt die Triggerzähler auf VerticalClock zurück
    @GET("/")
    fun setTriggerCounterToZero(@Query(NETWORK_ACTION) action: Int): Call<VCTriggerCounterResetResult>
}


