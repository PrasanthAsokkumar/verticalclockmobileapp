package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName


/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

class VCDateTimeResult(@SerializedName("value_0")
                       var timeHH : Int = 0,
                       @SerializedName("value_1")
                       var timeMM : Int = 0,
                       @SerializedName("value_2")
                       var dateDD : Int = 0,
                       @SerializedName("value_3")
                       var dateMM : Int = 0,
                       @SerializedName("value_4")
                       var dateYYYY : Int = 0,
                       @SerializedName("value_5")
                       var timeSS : Int = 0)
{
    fun getDateTime() : String {
        return "$dateDD-$dateMM-$dateYYYY $timeHH:$timeMM:$timeSS"
    }
}