package com.verticalclock.verticalclockapp

import android.app.Application
import android.content.Context

/**
 * Created by prasanthasokkumar on 05.02.18.
 */
class MainApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: MainApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }
}