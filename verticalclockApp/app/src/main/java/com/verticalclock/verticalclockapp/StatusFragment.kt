package com.verticalclock.verticalclockapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.provider.Settings
import android.util.Log
import android.view.*
import android.widget.Toast
import com.verticalclock.verticalclockapp.MainApplication
import com.verticalclock.verticalclockapp.R
import com.verticalclock.verticalclockapp.R.id.*
import com.verticalclock.verticalclockapp.helpers.*
import com.verticalclock.verticalclockapp.models.api.VCSensorResult
import kotlinx.android.synthetic.main.status_fragment.*
import java.util.*


/**
 * Created by Klaus Gerber on 18.02.2018.
 * Logic for the elements on the status-page
 */
class StatusFragment : Fragment() {
    private var refreshDataPending = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
        menu?.findItem(R.id.action_refresh)?.setVisible(false)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.status_fragment, null)
    }


    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Request to VerticalClock --> Refresh Data
        refreshDataPending = true
        tryRefreshData()

        // Swipe-Down Updater
        refreshStatus.setOnRefreshListener {
            // Request to VerticalClock --> Refresh Data
            refreshDataPending = true
            tryRefreshData()
        }
    }

    override fun onPause() {
        super.onPause()
        refreshStatus?.setRefreshing(false)
    }


    // Send a request to VeticalClock if the network settings are ok, and no other request ist pending
    fun tryRefreshData() {

        if (ConnectivityHelper().isWifiConnected() || SettingsHelper().isSimulationMode()) {
            // Busy, networkrequest not possible
            if (APIConnectorImpl.busy) {
                Log.d(getString(R.string.status_fragment), getString(R.string.new_request_try))
                refreshStatus?.isRefreshing = true
                val snackbar = Snackbar
                        .make(refreshStatus, getString(R.string.networkrequest_not_possible), 10000)
                        .setAction(getString(R.string.retry)) {
                            tryRefreshData()
                        }
                snackbar.setActionTextColor(Color.RED)
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (!APIConnectorImpl.busy) {
                            refreshStatus?.setRefreshing(false)
                        }
                    }
                })
                snackbar.show()
            } else {
                // Send the request according the preselection
                if (refreshDataPending) {
                    refreshData()
                }
            }
        } else {
            // Networkstatus not OK, inform user
            val snackbar = Snackbar
                    .make(refreshStatus, getString(R.string.networkstatus_nok), 10000)
                    .setAction(getString(R.string.ok)) {
                        val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                        startActivity(intent)
                    }
            snackbar.setActionTextColor(Color.RED)
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    super.onDismissed(snackbar, event)
                    if (!APIConnectorImpl.busy) {
                        refreshStatus?.setRefreshing(false)
                    }
                }
            })
            snackbar.show()
        }
    }


    // Makes the networkrequest if the pre-check was OK --> Refresh data
    fun refreshData() {
        refreshStatus?.isRefreshing = true
        if (SettingsHelper().isSimulationMode()) {
            val random = Random()
            tvMotionSensorActualState?.text = random.nextInt(2).toString()
            tvSoundSensorAcutalValue?.text = random.nextInt(1023).toString()
            tvLightSensorActualValue?.text = random.nextInt(1023).toString()
            refreshStatus?.setRefreshing(false)
            refreshDataPending = false
        } else {
            APIConnectorImpl.busy = true
            val call = APIConnectorImpl.api.getActualSensorValues(ApiActions.GetActualSensorValues.actionCode)
            call.enqueue(ApiCallback<VCSensorResult>({
                APIConnectorImpl.busy = false
                if (it != null) {
                    tvMotionSensorActualState?.text = "${it.motionSensorActMotionStatus}"
                    tvSoundSensorAcutalValue?.text = "${it.soundSensorActLevel}"
                    tvLightSensorActualValue?.text = "${it.lightSensorActBrightness}"
                } else {
                    connectionError()
                }
                refreshStatus?.setRefreshing(false)
                refreshDataPending = false
            }))
        }
    }

    // Show the user a Toast message if a networkrequest was not successful
    fun connectionError() {
        if (this.isVisible) {
            val toast = Toast.makeText(MainApplication.applicationContext(), getString(R.string.verticalclock_not_available), Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}