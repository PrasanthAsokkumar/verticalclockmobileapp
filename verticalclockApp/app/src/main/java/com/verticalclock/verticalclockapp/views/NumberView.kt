package com.verticalclock.verticalclockapp.views

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.verticalclock.verticalclockapp.R
import kotlinx.android.synthetic.main.view_number.view.*
import android.content.res.TypedArray
import com.verticalclock.verticalclockapp.MainApplication


/**
 * Created by prasanthasokkumar on 10.03.18.
 */

class NumberView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : RelativeLayout(context, attrs, defStyle, defStyleRes) {

    val animationDuration : Long = 2500

    val firstNumberPosition: Int = 668

    var lastNumber: Int = 9

    val differenz: Int = 472

    val toleranz: Int = 120


    init {
        var view = LayoutInflater.from(context)
                .inflate(R.layout.view_number, this, true)

        var layoutView = view.findViewById<View>(R.id.linearLayoutView)

        attrs?.let {

            val a = context.obtainStyledAttributes(it, R.styleable.custom_attributes)
            if (a.hasValue(R.styleable.custom_attributes_background_img_res_id))
            {
                var resourceId = a.getResourceId(R.styleable.custom_attributes_background_img_res_id, 0)
                this.numberView.setBackgroundResource(resourceId)
            }

            if (a.hasValue(R.styleable.custom_attributes_last_number))
            {
                this.lastNumber = a.getInteger(R.styleable.custom_attributes_last_number, 9)
            }

            a.recycle()
        }

        var yDown = 0f
        view.setOnTouchListener { innerView, motionEvent ->
            Log.d("setOnTouchListener", innerView.toString())

            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    Log.d("MOTION", "DOWN")
                    yDown = motionEvent.y
                }
                MotionEvent.ACTION_MOVE -> {
                    Log.d("MOTION", "MOVE")
                    val dist = motionEvent.y - yDown
                    yDown = motionEvent.y
                    val lp: RelativeLayout.LayoutParams = layoutView?.layoutParams as RelativeLayout.LayoutParams
                    lp.topMargin = Math.round(lp.topMargin + dist)

                    layoutView.layoutParams = lp
                }
                MotionEvent.ACTION_UP -> {
                    Log.d("MOTION", "UP")
                    //yDown = 0f

                    // DEBUG
                    val lp: RelativeLayout.LayoutParams = layoutView?.layoutParams as RelativeLayout.LayoutParams
                    Log.d("topMargin", lp.topMargin.toString())

                    adjustSelectedNumber()
                }
            }
            true
        }
    }

    fun setNumber(number: Int)
    {
        var targetMargin: Int

        if(number == -1) return

        if(number == 1) {
            targetMargin = firstNumberPosition
        } else if(number == 0)
        {
            if(this.lastNumber == 3)
            {
                targetMargin = firstNumberPosition - (this.differenz * 3)
            }else if(this.lastNumber == 5)
            {
                targetMargin = firstNumberPosition - (this.differenz * 5)
            }else
            {
                targetMargin = firstNumberPosition - (this.differenz * 9)
            }
        }else
        {
            targetMargin = firstNumberPosition - (this.differenz * (number - 1))
        }
        Log.d("NumberView - setNumber", targetMargin.toString())

        var layoutView = this.findViewById<View>(R.id.linearLayoutView)

        val animation = object : Animation() {

            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {

                val lp: RelativeLayout.LayoutParams = layoutView?.layoutParams as LayoutParams
                lp.topMargin = Math.round(targetMargin * interpolatedTime)

                if(lp.topMargin != 0)
                {
                    layoutView.layoutParams = lp
                }
            }
        }
        animation.duration = this.animationDuration
        layoutView?.startAnimation(animation)
    }

    fun setNumberWithoutAnimation(number: Int)
    {
        var targetMargin: Int

        if(number == -1) return

        if(number == 1)
        {
            targetMargin = firstNumberPosition
        } else if(number == 0)
        {
            if(this.lastNumber == 3)
            {
                targetMargin = firstNumberPosition - (this.differenz * 3)
            }else if(this.lastNumber == 5)
            {
                targetMargin = firstNumberPosition - (this.differenz * 5)
            }else
            {
                targetMargin = firstNumberPosition - (this.differenz * 9)
            }
        }else
        {
            targetMargin = firstNumberPosition - (this.differenz * (number - 1))
        }

        Log.d("NumberView - setNumber", targetMargin.toString())

        var layoutView = this.findViewById<View>(R.id.linearLayoutView)
        val lp: RelativeLayout.LayoutParams = layoutView?.layoutParams as LayoutParams
        lp.topMargin = targetMargin

        layoutView.layoutParams = lp
    }

    fun getSelectedNumber() : Int
    {

        Log.d("lastNumber - ", this.lastNumber.toString())

        var layoutView = this.findViewById<View>(R.id.linearLayoutView)
        val lp: RelativeLayout.LayoutParams = layoutView?.layoutParams as LayoutParams


        if( lp.topMargin >= (this.firstNumberPosition - this.toleranz) && lp.topMargin <= (this.firstNumberPosition + this.toleranz))
        {
            return 1
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 1) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 1) + this.toleranz) )
        {
            return 2
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 2) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 2) + this.toleranz) )
        {
            return 3
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 3) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 3) + this.toleranz) )
        {
            if(this.lastNumber == 3)
            {
                return 0
            }
            return 4
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 4) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 4) + this.toleranz) )
        {
            return 5
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 5) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 5) + this.toleranz) )
        {
            if(this.lastNumber == 5){
                return 0
            }
            else
            {
                return 6
            }
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 6) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 6) + this.toleranz) )
        {
            return 7
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 7) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 7) + this.toleranz) )
        {
            return 8
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 8) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 8) + this.toleranz) )
        {
            return 9
        }
        else if( lp.topMargin >= ((this.firstNumberPosition - this.differenz * 9) - this.toleranz) && lp.topMargin <= ((this.firstNumberPosition - this.differenz * 9) + this.toleranz) )
        {
            return 0
        }
        return -1
    }

    fun adjustSelectedNumber()
    {
        val selectedNumber = getSelectedNumber()
        if(selectedNumber <= this.lastNumber)
        {
            setNumberWithoutAnimation(selectedNumber)
        }
        Log.d("selectedNumber - ", selectedNumber.toString())
    }
}