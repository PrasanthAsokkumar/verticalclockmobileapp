package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCMotionSensorResult(@SerializedName("value_13")
                                var motionSensorActMotionStatus : Int = 0,
                                @SerializedName("value_14")
                                var motionSensorOnOff : Int = 0,
                                @SerializedName("value_15")
                                var motionSensorOffDelay : Int = 0)
{
}