package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCLightSensorResult(@SerializedName("value_0")
                                var lightSensorOnOff : Int = 0,
                                @SerializedName("value_1")
                                var lightSensorThreshold : Int = 0)
{
}