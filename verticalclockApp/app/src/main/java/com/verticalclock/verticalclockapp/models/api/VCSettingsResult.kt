package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName
import java.util.stream.Collectors.toMap

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCSettingsResult(@SerializedName("value_4")
                            var firmwareVersionHighLevelMC : String = "",
                            @SerializedName("value_5")
                            var firmwareVersionLowLevelMC : String = "",
                            @SerializedName("value_16")
                            var ipAddress : String = "")
{
    fun getSettings() : String {
        return "$firmwareVersionHighLevelMC-$firmwareVersionLowLevelMC-$ipAddress"
    }




}