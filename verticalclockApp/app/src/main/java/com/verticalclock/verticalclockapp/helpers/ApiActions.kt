package com.verticalclock.verticalclockapp.helpers

/**
 * Created by Klaus Gerber on 26.02.2018.
 * Definiert die Netzwerk-Request Codes für Vertical Clock
 */

enum class ApiActions(val actionCode: Int) {
    UndefinedMode(0),
    TimeMode(1),
    TimerMode(2),
    TemperatureMode(3),
    SetModeTemperature(10003),
    SetModeTime(10005),
    SetTimer(10006),
    GetTimer(10008),
    GetMode(10010),
    SetSoundSensorParameter(10020),
    SetLightSensorParameter(10025),
    SetMotionSensorParameter(10030),
    StartCalibration(10071),
    GetDateTime(10301),
    GetActualSensorValues(10301),
    GetSetting(10304),
    GetStatistics(10304),
    GetReferencingParameter(10304),
    SetTriggerCounterToZero(11001),
    SetReferencingParameter(11002)
}