package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCSoundSensorResult(@SerializedName("value_17")
                               var soundSensorOnOff : Int = 0,
                               @SerializedName("value_18")
                               var soundSensorOffDelay : Int = 0,
                               @SerializedName("value_19")
                               var soundSensorThreshold : Int = 0,
                               @SerializedName("value_20")
                               var soundSensorThresholdClap: Int = 0)
{
}