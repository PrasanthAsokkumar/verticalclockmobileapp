package com.verticalclock.verticalclockapp

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.settings_fragment.*
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.Toast
import com.verticalclock.verticalclockapp.helpers.*
import com.verticalclock.verticalclockapp.models.api.*


/**
 * Created by Klaus Gerber on 18.02.2018.
 * Logic for the elements on the settings-page
 */
class SettingsFragment : Fragment() {
    private val settingsHelper = SettingsHelper()

    private var motionSensorOffDelayOld: String = ""
    private var motionSensorParameterChanged = false

    private var soundSensorOffDelayOld: String = ""
    private var soundSensorOnThresholdOld: String = ""
    private var soundSensorOnThresholdClapOld: String = ""
    private var soundSensorParameterChanged = false

    private var lightSensorOnThresholdOld: String = ""
    private var lightSensorParameterChanged = false

    private var networkParameteripAdressOld: String = ""
    private var networkParameterChanged = false

    private var referencingAfterDaysOld: String = ""
    private var referencingParameterChanged = false
    private var drawable: Drawable? = null

    private var switchStatesLoaded = false

    private var refreshDataPending = false
    private var sendSettingsPending = false
    private var calibratingPending = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
        drawable = menu?.findItem(R.id.action_refresh)?.icon
        drawable?.mutate()
        switchStatesLoaded = true
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.settings_fragment, null)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        etIPAdress.setText(settingsHelper.getIPAddress()!!.subSequence(7, settingsHelper.getIPAddress()!!.lastIndex + 1))

        //Store actual values from edit text
        motionSensorOffDelayOld = etMotionSensorOffDelay.text.toString()
        soundSensorOffDelayOld = etSoundSensorOffDelay.text.toString()
        soundSensorOnThresholdOld = etSoundSensorOnThreshold.text.toString()
        soundSensorOnThresholdClapOld = etSoundSensorClapThreshold.text.toString()
        lightSensorOnThresholdOld = etLightSensorOnThreshold.text.toString()
        referencingAfterDaysOld = etReferencingAfterDays.text.toString()
        networkParameteripAdressOld = etIPAdress.text.toString()

        // Request to VerticalClock --> Refresh Data
        refreshDataPending = true
        tryRefreshData()

        // Swipe-Down Updater
        refreshSettings.setOnRefreshListener {
            // Request to VerticalClock --> Refresh Data
            refreshDataPending = true
            tryRefreshData()
        }


        //******************** Motion Sensor Fields ************************************************
        swMotionSensor.setOnCheckedChangeListener({ buttonView, isChecked ->
            Log.d("setOnCheckedChangeListener", buttonView.toString())
            Log.d("setOnCheckedChangeListener", isChecked.toString())
            if (switchStatesLoaded) {
                motionSensorParameterChanged = true
                drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
            }
        })

        etMotionSensorOffDelay.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (motionSensorOffDelayOld != "-" && motionSensorOffDelayOld != etMotionSensorOffDelay.text.toString()) {
                    motionSensorParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                motionSensorOffDelayOld = etMotionSensorOffDelay.text.toString()
            }
        })

        //******************** Sound Sensor Fields *************************************************
        swSoundSensor.setOnCheckedChangeListener({ buttonView, isChecked ->
            Log.d("setOnCheckedChangeListener", buttonView.toString())
            Log.d("setOnCheckedChangeListener", isChecked.toString())
            if (switchStatesLoaded) {
                soundSensorParameterChanged = true
                drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
            }
        })

        etSoundSensorOffDelay.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (soundSensorOffDelayOld != "-" && soundSensorOffDelayOld != etSoundSensorOffDelay.text.toString()) {
                    soundSensorParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                soundSensorOffDelayOld = etSoundSensorOffDelay.text.toString()
            }
        })


        etSoundSensorOnThreshold.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (soundSensorOnThresholdOld != "-" && soundSensorOnThresholdOld != etSoundSensorOnThreshold.text.toString()) {
                    soundSensorParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                soundSensorOnThresholdOld = etSoundSensorOnThreshold.text.toString()
            }
        })


        etSoundSensorClapThreshold.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (soundSensorOnThresholdClapOld != "-" && soundSensorOnThresholdClapOld != etSoundSensorClapThreshold.text.toString()) {
                    soundSensorParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                soundSensorOnThresholdClapOld = etSoundSensorClapThreshold.text.toString()
            }
        })

        //******************** Light Sensor Fields *************************************************
        swLightSensor.setOnCheckedChangeListener({ buttonView, isChecked ->
            Log.d("setOnCheckedChangeListener", buttonView.toString())
            Log.d("setOnCheckedChangeListener", isChecked.toString())
            if (switchStatesLoaded) {
                lightSensorParameterChanged = true
                drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
            }
        })

        etLightSensorOnThreshold.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (lightSensorOnThresholdOld != "-" && lightSensorOnThresholdOld != etLightSensorOnThreshold.text.toString()) {
                    lightSensorParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                lightSensorOnThresholdOld = etLightSensorOnThreshold.text.toString()
            }
        })


        //******************** Referencing Fields **************************************************
        etReferencingAfterDays.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (referencingAfterDaysOld != "-" && referencingAfterDaysOld != etReferencingAfterDays.text.toString()) {
                    referencingParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                referencingAfterDaysOld = etReferencingAfterDays.text.toString()
            }
        })

        btnReferencing.setOnClickListener {
            if (!SettingsHelper().isSimulationMode()) {
                // Request to VerticalClock --> Calibrating (Referencing)
                calibratingPending = true
                tryRefreshData()

            }
        }


        //******************** IP-Adress ****Fields ************************************************
        etIPAdress.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int,
                                           count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int,
                                       before: Int, count: Int) {
                if (networkParameteripAdressOld != "-" && networkParameteripAdressOld != etIPAdress.text.toString()) {
                    networkParameterChanged = true
                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                }
                networkParameteripAdressOld = etIPAdress.text.toString()
            }
        })


        //******************** Simulation-Mode Fields **********************************************
        swSimulationMode.setOnCheckedChangeListener({ buttonView, isChecked ->
            Log.d("setOnCheckedChangeListener", buttonView.toString())
            Log.d("setOnCheckedChangeListener", isChecked.toString())
            if (switchStatesLoaded) {
                settingsHelper.setSimulationMode(swSimulationMode.isChecked)
            }
        })
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_refresh) {

            if (motionSensorParameterChanged || soundSensorParameterChanged || lightSensorParameterChanged || referencingParameterChanged || networkParameterChanged) {
                // Request to VerticalClock --> Set changed settings
                sendSettingsPending = true
                tryRefreshData()
            }
        }
        return false
    }

    override fun onPause() {
        super.onPause()
        refreshSettings?.setRefreshing(false)
    }

    // Send a request to VeticalClock if the network settings are ok, and no other request ist pending
    fun tryRefreshData() {

        if (ConnectivityHelper().isWifiConnected() || SettingsHelper().isSimulationMode()) {
            // Busy, networkrequest not possible
            if (APIConnectorImpl.busy) {
                Log.d(getString(R.string.settings_fragment), getString(R.string.new_request_try))
                refreshSettings?.isRefreshing = true
                val snackbar = Snackbar
                        .make(refreshSettings, getString(R.string.networkrequest_not_possible), 10000)
                        .setAction(getString(R.string.retry)) {
                            tryRefreshData()
                        }
                snackbar.setActionTextColor(Color.RED)
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (!APIConnectorImpl.busy) {
                            refreshSettings?.setRefreshing(false)
                        }
                    }
                })
                snackbar.show()

            } else {
                // Send the request according the preselection
                if (refreshDataPending) {
                    refreshData()
                } else if (sendSettingsPending) {
                    sendNewSettings()
                } else if (calibratingPending) {
                    setCalibration()
                }
            }
        } else {
            // Networkstatus not OK, inform user
            val snackbar = Snackbar
                    .make(refreshSettings, getString(R.string.networkstatus_nok), 10000)
                    .setAction(getString(R.string.ok)) {
                        val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                        startActivity(intent)
                    }
            snackbar.setActionTextColor(Color.RED)
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    super.onDismissed(snackbar, event)
                    if (!APIConnectorImpl.busy) {
                        refreshSettings?.setRefreshing(false)
                    }
                }
            })
            snackbar.show()
        }
    }

    // Makes the request to VerticalClock according the changed parameters
    fun sendNewSettings() {
        if ((!motionSensorParameterChanged && !soundSensorParameterChanged && !lightSensorParameterChanged && !referencingParameterChanged) || SettingsHelper().isSimulationMode()) {

            refreshDataPending = true
            sendSettingsPending = false
            tryRefreshData()
            drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
        }

        if (!SettingsHelper().isSimulationMode()) {
            if (motionSensorParameterChanged) {
                motionSensorParameterChanged = false
                setMotionSensorParameter()
            }
            if (soundSensorParameterChanged) {
                soundSensorParameterChanged = false
                setSoundSensorParameter()
            }
            if (lightSensorParameterChanged) {
                lightSensorParameterChanged = false
                setLightSensorParameter()
            }
            if (referencingParameterChanged) {
                referencingParameterChanged = false
                setReferencingParameter()
            }
            if (networkParameterChanged) {
                networkParameterChanged = false
                setNetworkParameter()
            }
        } else {
            motionSensorParameterChanged = false
            soundSensorParameterChanged = false
            referencingParameterChanged = false
            networkParameterChanged = false
        }
    }

    // Makes the networkrequest if the pre-check was OK --> Refresh data
    fun refreshData() {
        if (SettingsHelper().isSimulationMode()) {
            if (refreshSettings != null) {
                swSimulationMode?.isChecked = settingsHelper.isSimulationMode()
                refreshSettings?.setRefreshing(false)
                drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
                refreshDataPending = false
            }
        } else {
            APIConnectorImpl.busy = true
            refreshSettings?.isRefreshing = true
            val callPartOne = APIConnectorImpl.api.getActualSensorValues(ApiActions.GetActualSensorValues.actionCode)
            callPartOne.enqueue(ApiCallback<VCSensorResult>({

                if (it != null) {
                    swMotionSensor?.isChecked = it.motionSensorOnOff == 1
                    etMotionSensorOffDelay?.setText(it.motionSensorOffDelay.toString())

                    swSoundSensor?.isChecked = it.soundSensorOnOff == 1
                    etSoundSensorOffDelay?.setText(it.soundSensorOffDelay.toString())
                    etSoundSensorOnThreshold?.setText(it.soundSensorThreshold.toString())
                    etSoundSensorClapThreshold?.setText(it.soundSensorThresholdClap.toString())

                    swLightSensor?.isChecked = it.lightSensorOnOff == 1
                    etLightSensorOnThreshold?.setText(it.lightSensorThreshold.toString())

                    swSimulationMode?.isChecked = settingsHelper.isSimulationMode()

                    refreshSettings?.isRefreshing = true
                    val callPartTwo = APIConnectorImpl.api.getReferencingParameter(ApiActions.GetReferencingParameter.actionCode)
                    callPartTwo.enqueue(ApiCallback<VCGetReferencingParameterResult>({
                        APIConnectorImpl.busy = false
                        if (it != null) {
                            etReferencingAfterDays?.setText(it.referencingAfterDays.toString())
                        } else {
                            connectionError()
                        }
                        refreshSettings?.setRefreshing(false)
                        drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
                        refreshDataPending = false
                    }))
                } else {
                    connectionError()
                    APIConnectorImpl.busy = false
                    refreshSettings?.setRefreshing(false)
                    drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
                    refreshDataPending = false
                }
            }))
        }
    }

    // Makes the networkrequest if the pre-check was OK --> Calibrating VerticalClock
    fun setCalibration() {
        refreshSettings?.isRefreshing = true
        APIConnectorImpl.busy = true
        val call = APIConnectorImpl.api.setCalibration(ApiActions.StartCalibration.actionCode)
        call.enqueue(ApiCallback<VCCalibrationResult>({
            APIConnectorImpl.busy = false
            if (it == null) {
                connectionError()
            }
            refreshSettings?.setRefreshing(false)
            calibratingPending = false
        }))
    }

    // Makes the networkrequest if the pre-check was OK --> Sets the motion sensor parameter
    fun setMotionSensorParameter() {
        var switchState = "0"
        val offDelay = etMotionSensorOffDelay.text.toString()
        if (swMotionSensor.isChecked) {
            switchState = "1"
        }
        APIConnectorImpl.busy = true
        refreshSettings?.isRefreshing = true
        val call = APIConnectorImpl.api.setMotionSoundParameter(switchState, offDelay, ApiActions.SetMotionSensorParameter.actionCode)
        call.enqueue(ApiCallback<VCMotionSensorResult>({
            APIConnectorImpl.busy = false
            if (it != null) {
                sendNewSettings()
            } else {
                connectionError()
                refreshSettings?.setRefreshing(false)
            }
        }))
    }

    // Makes the networkrequest if the pre-check was OK --> Sets the sound sensor parameter
    fun setSoundSensorParameter() {
        var switchState = "0"
        val offDelay = etSoundSensorOffDelay.text.toString()
        val onThreshold = etSoundSensorOnThreshold.text.toString()
        val clapThreshold = etSoundSensorClapThreshold.text.toString()

        if (swSoundSensor.isChecked) {
            switchState = "1"
        }
        APIConnectorImpl.busy = true
        refreshSettings?.isRefreshing = true
        val call = APIConnectorImpl.api.setSoundSensorParameter(switchState, offDelay, onThreshold, clapThreshold, ApiActions.SetSoundSensorParameter.actionCode)
        call.enqueue(ApiCallback<VCSoundSensorResult>({
            APIConnectorImpl.busy = false
            if (it != null) {
                sendNewSettings()
            } else {
                connectionError()
                refreshSettings?.setRefreshing(false)
            }
        }))
    }

    // Makes the networkrequest if the pre-check was OK --> Sets the light sensor parameter
    fun setLightSensorParameter() {
        var switchState = "0"
        val onThreshold = etLightSensorOnThreshold.text.toString()

        if (swLightSensor.isChecked) {
            switchState = "1"
        }
        APIConnectorImpl.busy = true
        refreshSettings?.isRefreshing = true
        val call = APIConnectorImpl.api.setLightSensorParameter(switchState, onThreshold, ApiActions.SetLightSensorParameter.actionCode)
        call.enqueue(ApiCallback<VCLightSensorResult>({
            APIConnectorImpl.busy = false
            if (it != null) {
                sendNewSettings()
            } else {
                connectionError()
                refreshSettings?.setRefreshing(false)
            }
        }))
    }

    // Makes the networkrequest if the pre-check was OK --> Sets the referencing (calibrating) parameter
    fun setReferencingParameter() {
        val referencingAfterDays = etReferencingAfterDays.text.toString()
        APIConnectorImpl.busy = true
        refreshSettings?.isRefreshing = true
        val call = APIConnectorImpl.api.setReferencingParameter(referencingAfterDays, ApiActions.SetReferencingParameter.actionCode)
        call.enqueue(ApiCallback<VCSetReferencingParamResult>({
            APIConnectorImpl.busy = false
            if (it != null) {
                sendNewSettings()
            } else {
                connectionError()
                refreshSettings?.setRefreshing(false)
            }
        }))
    }

    // Sets the IP-Adress
    fun setNetworkParameter() {
        settingsHelper.setIPAddress(etIPAdress.text.toString())
        APIConnectorImpl.setIp()
        sendNewSettings()
    }


    // Show the user a Toast message if a networkrequest was not successful
    fun connectionError() {
        if (this.isVisible) {
            val toast = Toast.makeText(MainApplication.applicationContext(), getString(R.string.verticalclock_not_available), Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}