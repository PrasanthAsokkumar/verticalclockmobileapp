package com.verticalclock.verticalclockapp


import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.*
import com.verticalclock.verticalclockapp.helpers.SettingsHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*



class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var settingsHelper: SettingsHelper = SettingsHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!settingsHelper.isAppInitialized()) {
            val onboarding = Intent(this, OnboardingActivity::class.java)
            startActivity(onboarding)
            finish()
            return
        }

        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        this.setTitle(R.string.menu_mode)
        displayScreen(R.id.nav_mode)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else if(settingsHelper.isAppInitialized()) {

            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(intent)
        }else{
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        if (item.itemId == R.id.nav_feedback)
        {
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.type = "message/rfc822" // android.view.HTTP.PLAIN_TEXT_TYPE
            emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf("kgerber1@gmx.ch"))
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "VerticalClockApp")
            startActivity(emailIntent)
        } else {
            displayScreen(item.itemId)
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    // Blendet das entsprechend der Auswahl im NavigationDrawer ein zugehöriges Fragment ein
    fun displayScreen(id: Int) {
        val fragment = when (id) {
            R.id.nav_mode -> {
                this.setTitle(getString(R.string.menu_mode))
                VerticalClockFragment()
            }
            R.id.nav_status -> {
                this.setTitle(R.string.menu_status)
                StatusFragment()
            }
            R.id.nav_settings -> {
                this.setTitle(R.string.menu_settings)
                SettingsFragment()
            }
            R.id.nav_statistics -> {
                this.setTitle(R.string.menu_statistics)
                StatisticsFragment()
            }
            else -> {
                VerticalClockFragment()
            }
        }
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }
}
