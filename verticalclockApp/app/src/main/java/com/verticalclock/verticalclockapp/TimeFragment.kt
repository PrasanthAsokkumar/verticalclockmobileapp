package com.verticalclock.verticalclockapp

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Toast
import com.verticalclock.verticalclockapp.R.id.refreshTime
import com.verticalclock.verticalclockapp.R.id.timeView
import com.verticalclock.verticalclockapp.helpers.*
import com.verticalclock.verticalclockapp.models.api.VCDateTimeResult
import com.verticalclock.verticalclockapp.models.api.VCModeGetResult
import com.verticalclock.verticalclockapp.models.api.VCModeSetResult
import kotlinx.android.synthetic.main.time_fragment.*
import java.lang.Integer.max
import java.lang.Integer.min


/**
 * Created by Klaus Gerber on 18.02.2018.
 * Logic for the elements on the time-page
 */
class TimeFragment : Fragment() {
    private var drawable: Drawable? = null
    private var refreshDataPending = false
    private var firstAnimationOver = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
        drawable = menu?.findItem(R.id.action_refresh)?.icon
        drawable?.mutate()

        // Request to VerticalClock --> Refresh Data
        firstAnimationOver = false
        refreshDataPending = true
        tryRefreshData()
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.time_fragment, null)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        // Swipe-Down Updater
        refreshTime.setOnRefreshListener {
            // Request to VerticalClock --> Refresh Data
            refreshDataPending = true
            tryRefreshData()
        }
    }

    override fun onPause() {
        super.onPause()
        refreshTime?.setRefreshing(false)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_refresh) {

            // Request to VerticalClock --> Set the time mode
            if (!APIConnectorImpl.busy) {
                drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)


                if (!SettingsHelper().isSimulationMode()) {
                    refreshTime?.isRefreshing = true
                    APIConnectorImpl.busy = true
                    val call = APIConnectorImpl.api.setMode(ApiActions.SetModeTime.actionCode)
                    call.enqueue(ApiCallback<VCModeSetResult>({
                        APIConnectorImpl.busy = false
                        if (refreshTime != null) {
                            refreshTime?.setRefreshing(false)
                        } else {
                            connectionError()
                        }
                    }))
                }
            }
        }
        return false
    }


    // Send a request to VeticalClock if the network settings are ok, and no other request ist pending
    fun tryRefreshData() {

        if (ConnectivityHelper().isWifiConnected() || SettingsHelper().isSimulationMode()) {
            // Busy, networkrequest not possible
            if (APIConnectorImpl.busy) {
                Log.d(getString(R.string.time_fragment), getString(R.string.new_request_try))
                refreshTime?.isRefreshing = true
                val snackbar = Snackbar
                        .make(refreshTime, getString(R.string.networkrequest_not_possible), 10000)
                        .setAction(getString(R.string.retry)) {
                            tryRefreshData()
                        }
                snackbar.setActionTextColor(Color.RED)
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (!APIConnectorImpl.busy) {
                            refreshTime?.setRefreshing(false)
                        }
                    }
                })
                snackbar.show()
            } else {
                // Send the request according the preselection
                if (refreshDataPending) {
                    refreshData()
                }
            }
        } else {
            // Networkstatus not OK, inform user
            val snackbar = Snackbar
                    .make(refreshTime, getString(R.string.networkstatus_nok), 10000)
                    .setAction(getString(R.string.ok)) {
                        val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                        startActivity(intent)
                    }
            snackbar.setActionTextColor(Color.RED)
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    super.onDismissed(snackbar, event)
                    if (!APIConnectorImpl.busy) {
                        refreshTime?.setRefreshing(false)
                    }
                }
            })
            snackbar.show()
        }
    }


    // Makes the networkrequest if the pre-check was OK --> Refresh data
    fun refreshData() {
        refreshTime?.isRefreshing = true

        if (SettingsHelper().isSimulationMode()) {
            val calendar = Calendar.getInstance()
            val timeformat = SimpleDateFormat("HH:mm")
            timeView?.setTime(timeformat.format(calendar.time))
            refreshTime?.setRefreshing(false)
            refreshDataPending = false
        } else {
            APIConnectorImpl.busy = true
            val callPartOne = APIConnectorImpl.api.getActualDateAndTime(ApiActions.GetDateTime.actionCode)
            callPartOne.enqueue(ApiCallback<VCDateTimeResult>({
                APIConnectorImpl.busy = false
                if (it != null) {
                    if (timeView != null) {
                        if (!firstAnimationOver) {
                            timeView.setTime("${String.format("%02d", min(max(it.timeHH, 0), 39))}:${String.format("%02d", min(max(it.timeMM, 0), 59))}")
                        } else {
                            timeView.setTimeWithoutAnimation("${String.format("%02d", min(max(it.timeHH, 0), 39))}:${String.format("%02d", min(max(it.timeMM, 0), 59))}")
                        }
                        firstAnimationOver = true

                        refreshTime?.isRefreshing = true
                        val callPartTwo = APIConnectorImpl.api.getMode(ApiActions.GetMode.actionCode)
                        callPartTwo.enqueue(ApiCallback<VCModeGetResult>({
                            APIConnectorImpl.busy = false
                            if (it != null) {
                                val actualMode = it.mode
                                if (actualMode != ApiActions.TimeMode.actionCode) {
                                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                                }
                            } else {
                                connectionError()
                            }
                            refreshTime?.setRefreshing(false)
                        }))
                    }

                } else {
                    connectionError()
                }
                refreshTime?.setRefreshing(false)
                refreshDataPending = false
            }))
        }
    }

    // Show the user a Toast message if a networkrequest was not successful
    fun connectionError() {
        if (this.isVisible) {
            val toast = Toast.makeText(MainApplication.applicationContext(), getString(R.string.verticalclock_not_available), Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}