package com.verticalclock.verticalclockapp.helpers

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Klaus Gerber on 16.03.2018.
 * Retrofit Instanz für Netzwerkrequests auf VerticalClock
 */
object APIConnectorImpl {
    var busy = false
    var api: APIConnector
    init {
        val builder = Retrofit.Builder()
                .baseUrl(SettingsHelper().getIPAddress()!!)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        api = builder.create(APIConnector::class.java)
    }

    fun setIp(){
        val builder = Retrofit.Builder()
                .baseUrl(SettingsHelper().getIPAddress()!!)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        api = builder.create(APIConnector::class.java)
    }
}