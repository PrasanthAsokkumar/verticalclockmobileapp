package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCTriggerCounterResetResult(@SerializedName("value_0")
                         var TrigerResetOk : Int = 0)
{
}