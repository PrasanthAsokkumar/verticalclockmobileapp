package com.verticalclock.verticalclockapp.models.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Klaus Gerber on 12.02.2018.
 * Definiert die Rückantwort auf den jeweiligen Netzwerkrequest
 */

data class VCTimerGetResult(@SerializedName("value_0")
                            var timerHH : Int = 0,
                            @SerializedName("value_1")
                            var timerMM : Int = 0,
                            @SerializedName("value_2")
                            var timerSS : Int = 0)
{
}