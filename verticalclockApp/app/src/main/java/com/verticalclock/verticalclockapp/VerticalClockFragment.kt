package com.verticalclock.verticalclockapp

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.verticalclock.verticalclockapp.*
import kotlinx.android.synthetic.main.verticalclock_fragment.*



/**
 * Created by Klaus Gerber on 18.02.2018.
 * Manages the framents for the time, temperature and timer-page
 */
class VerticalClockFragment : Fragment() {

    private var mSectionsPagerAdapter: VerticalClockFragment.SectionsPagerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater?.inflate(R.layout.verticalclock_fragment, null)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(childFragmentManager)


        // Set up the ViewPager with the sections adapter.
        vpContainerVerticalClockFragment.adapter = mSectionsPagerAdapter


        vpContainerVerticalClockFragment.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(vpContainerVerticalClockFragment))
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            when (position) {
                    0 -> {
                        Log.d("SectionsPagerAdapter:", "TimeFragment")
                        return TimeFragment()
                    }
                    1 -> {
                        Log.d("SectionsPagerAdapter:", "TemperatureFragment")
                        return TemperatureFragment()
                    }
                    2 -> {
                        Log.d("SectionsPagerAdapter:", "TimerFragment")
                        return TimerFragment()
                    }
                    else -> {
                        Log.d("SectionsPagerAdapter:", "TimeFragment Else!!!!!")
                        return TimeFragment()
                    }
                }
        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }
    }
}