package com.verticalclock.verticalclockapp

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.widget.Toast
import com.verticalclock.verticalclockapp.helpers.*
import com.verticalclock.verticalclockapp.models.api.VCTimerGetResult
import com.verticalclock.verticalclockapp.models.api.VCTimerSetResult
import kotlinx.android.synthetic.main.timer_fragment.*
import android.graphics.drawable.Drawable
import android.provider.Settings
import android.support.design.widget.Snackbar
import java.util.*
import android.graphics.PorterDuff
import com.verticalclock.verticalclockapp.R.id.refreshTimer
import com.verticalclock.verticalclockapp.R.id.timerView
import com.verticalclock.verticalclockapp.models.api.VCModeGetResult


/**
 * Created by Klaus Gerber on 18.02.2018.
 * Logic for the elements on the timer-page  //ToDO Swipe layout funktioniert nicht zusammen mit Zahlen swipen!!!!!!!!!!!!!!!
 */
class TimerFragment : Fragment() {
    private var drawable: Drawable? = null
    private var refreshDataPending = false
    private var firstAnimationOver = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.main, menu)
        drawable = menu?.findItem(R.id.action_refresh)?.icon
        drawable?.mutate()

        // Request to VerticalClock --> Refresh Data
        firstAnimationOver = false
        refreshDataPending = true
        tryRefreshData()
        refreshTimer.setEnabled(false);
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.timer_fragment, null)

    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Swipe-Down Updater
        refreshTimer.setOnRefreshListener {
            // Request to VerticalClock --> Refresh Data
            refreshDataPending = true
            tryRefreshData()
        }
    }

    override fun onPause() {
        super.onPause()
        refreshTimer?.setRefreshing(false)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_refresh) {

            // Request to VerticalClock --> Set the temperature mode
            if (!APIConnectorImpl.busy) {
                drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)

                if (!SettingsHelper().isSimulationMode()) {
                    refreshTimer?.isRefreshing = true
                    APIConnectorImpl.busy = true

                    val call = APIConnectorImpl.api.setTimer(timerView.getTime(), ApiActions.SetTimer.actionCode)
                    call.enqueue(ApiCallback<VCTimerSetResult>({
                        APIConnectorImpl.busy = false
                        if (refreshTimer != null) {
                            refreshTimer?.setRefreshing(false)
                        } else {
                            connectionError()
                        }
                    }))
                }
            }
        }
        return false
    }


    // Send a request to VeticalClock if the network settings are ok, and no other request ist pending
    fun tryRefreshData() {

        if (ConnectivityHelper().isWifiConnected() || SettingsHelper().isSimulationMode()) {
            // Busy, networkrequest not possible
            if (APIConnectorImpl.busy) {
                Log.d(getString(R.string.timer_fagment), getString(R.string.new_request_try))
                refreshTimer?.isRefreshing = true
                val snackbar = Snackbar
                        .make(refreshTimer, getString(R.string.networkrequest_not_possible), 10000)
                        .setAction(getString(R.string.retry)) {
                            tryRefreshData()
                        }
                snackbar.setActionTextColor(Color.RED)
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {
                        super.onDismissed(snackbar, event)
                        if (!APIConnectorImpl.busy) {
                            refreshTimer?.setRefreshing(false)
                        }
                    }
                })
                snackbar.show()

            } else {
                // Send the request according the preselection
                if (refreshDataPending) {
                    refreshData()
                }
            }
        } else {
            // Networkstatus not OK, inform user
            val snackbar = Snackbar
                    .make(refreshTimer, getString(R.string.networkstatus_nok), 10000)
                    .setAction(getString(R.string.ok)) {
                        val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                        startActivity(intent)
                    }
            snackbar.setActionTextColor(Color.RED)
            snackbar.addCallback(object : Snackbar.Callback() {
                override fun onDismissed(snackbar: Snackbar?, event: Int) {
                    super.onDismissed(snackbar, event)
                    if (!APIConnectorImpl.busy) {
                        refreshTimer?.setRefreshing(false)
                    }
                }
            })
            snackbar.show()
        }
    }


    // Makes the networkrequest if the pre-check was OK --> Refresh data
    fun refreshData() {
        refreshTimer?.isRefreshing = true
        drawable?.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)

        if (SettingsHelper().isSimulationMode()) {
            val random = Random()
            val hh = random.nextInt(23)
            val mm = random.nextInt(59)
            timerView?.setTime("${String.format("%02d", hh)}:${String.format("%02d", mm)}")
            refreshTimer?.setRefreshing(false)
            refreshDataPending = false
        } else {
            APIConnectorImpl.busy = true
            val callPartOne = APIConnectorImpl.api.getTimer(ApiActions.GetTimer.actionCode)
            callPartOne.enqueue(ApiCallback<VCTimerGetResult>({
                APIConnectorImpl.busy = false
                if (it != null) {

                    if (timerView != null) {
                        if (!firstAnimationOver) {
                            timerView.setTime("${String.format("%02d", Integer.min(Integer.max(it.timerHH, 0), 39))}:${String.format("%02d", Integer.min(Integer.max(it.timerMM, 0), 59))}")
                        } else {
                            timerView.setTimeWithoutAnimation("${String.format("%02d", Integer.min(Integer.max(it.timerHH, 0), 39))}:${String.format("%02d", Integer.min(Integer.max(it.timerMM, 0), 59))}")
                        }
                        firstAnimationOver = true

                        refreshTimer?.isRefreshing = true
                        val callPartTwo = APIConnectorImpl.api.getMode(ApiActions.GetMode.actionCode)
                        callPartTwo.enqueue(ApiCallback<VCModeGetResult>({
                            APIConnectorImpl.busy = false
                            if (it != null) {
                                val actualMode = it.mode
                                if (actualMode != ApiActions.TimerMode.actionCode && actualMode != ApiActions.UndefinedMode.actionCode) {
                                    drawable?.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP)
                                }
                            } else {
                                connectionError()
                            }
                            refreshTimer?.setRefreshing(false)
                        }))
                    }

                } else {
                    connectionError()
                }
                refreshTimer?.setRefreshing(false)
                refreshDataPending = false
            }))
        }
    }

    // Show the user a Toast message if a networkrequest was not successful
    fun connectionError() {
        if (this.isVisible) {
            val toast = Toast.makeText(MainApplication.applicationContext(), getString(R.string.verticalclock_not_available), Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}