package com.verticalclock.verticalclockapp.views

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.verticalclock.verticalclockapp.R
import kotlinx.android.synthetic.main.view_number.view.*
import java.util.*

/**
 * Created by prasanthasokkumar on 11.03.18.
 */

class VerticalClockView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : RelativeLayout(context, attrs, defStyle, defStyleRes)
{
    init
    {
        var view = LayoutInflater.from(context)
                .inflate(R.layout.view_vertical_clock, this, true)

        attrs?.let {

            val a = context.obtainStyledAttributes(it, R.styleable.custom_attributes)
            if (a.hasValue(R.styleable.custom_attributes_touch_disabled))
            {
                var touchDisabled: Boolean = a.getBoolean(R.styleable.custom_attributes_touch_disabled, false)

                view.findViewById<View>(R.id.numberViewOne)?.isEnabled = !touchDisabled
                view.findViewById<View>(R.id.numberViewTwo)?.isEnabled = !touchDisabled
                view.findViewById<View>(R.id.numberViewTree)?.isEnabled = !touchDisabled
                view.findViewById<View>(R.id.numberViewFour)?.isEnabled = !touchDisabled
            }

            a.recycle()
        }
    }



    fun setNumber(numberOne: Int, numberTwo: Int, numberTree: Int, numberFour: Int)
    {
        var numberViewOne = findViewById<NumberView>(R.id.numberViewOne)
        numberViewOne?.setNumber(numberOne)

        var numberViewTwo = findViewById<NumberView>(R.id.numberViewTwo)
        numberViewTwo?.setNumber(numberTwo)

        var numberViewTree = findViewById<NumberView>(R.id.numberViewTree)
        numberViewTree?.setNumber(numberTree)

        var numberViewFour = findViewById<NumberView>(R.id.numberViewFour)
        numberViewFour?.setNumber(numberFour)
    }

    fun setNumberWithoutAnimation(numberOne: Int, numberTwo: Int, numberTree: Int, numberFour: Int)
    {
        var numberViewOne = findViewById<NumberView>(R.id.numberViewOne)
        numberViewOne?.setNumberWithoutAnimation(numberOne)

        var numberViewTwo = findViewById<NumberView>(R.id.numberViewTwo)
        numberViewTwo?.setNumberWithoutAnimation(numberTwo)

        var numberViewTree = findViewById<NumberView>(R.id.numberViewTree)
        numberViewTree?.setNumberWithoutAnimation(numberTree)

        var numberViewFour = findViewById<NumberView>(R.id.numberViewFour)
        numberViewFour?.setNumberWithoutAnimation(numberFour)
    }

    fun setTime (time: String)
    {
        Log.d("VerticalClockView - setTime", time)
        setNumber(time.get(0).toString().toInt(), time.get(1).toString().toInt(), time.get(3).toString().toInt(), time.get(4).toString().toInt())
    }

    fun setTimeWithoutAnimation (time: String)
    {
        Log.d("VerticalClockView - setTime", time)
        setNumberWithoutAnimation(time.get(0).toString().toInt(), time.get(1).toString().toInt(), time.get(3).toString().toInt(), time.get(4).toString().toInt())
    }

    fun getTime () : String
    {
        var numberViewOne = this.findViewById<NumberView>(R.id.numberViewOne)
        var numberViewTwo = this.findViewById<NumberView>(R.id.numberViewTwo)
        var numberViewTree = this.findViewById<NumberView>(R.id.numberViewTree)
        var numberViewFour = this.findViewById<NumberView>(R.id.numberViewFour)

        val time = numberViewOne?.getSelectedNumber().toString() + numberViewTwo?.getSelectedNumber().toString() + ":" + numberViewTree?.getSelectedNumber().toString() + numberViewFour?.getSelectedNumber().toString()
        return time
    }
}