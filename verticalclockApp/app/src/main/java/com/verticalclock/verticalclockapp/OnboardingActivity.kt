package com.verticalclock.verticalclockapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager


class OnboardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        supportActionBar?.hide()

        val viewPager = findViewById<ViewPager>(R.id.viewpager) as ViewPager
        viewPager.adapter = OnboardingPagerAdapter()
    }
}
