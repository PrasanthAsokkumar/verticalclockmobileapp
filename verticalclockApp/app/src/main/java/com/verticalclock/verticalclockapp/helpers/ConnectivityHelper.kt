package com.verticalclock.verticalclockapp.helpers

import android.content.Context
import android.net.wifi.SupplicantState
import android.net.wifi.WifiManager
import com.verticalclock.verticalclockapp.MainApplication

/**
 * Created by prasanthasokkumar on 19.03.18.
 */
class ConnectivityHelper {

    fun isWifiConnected() : Boolean
    {
        val wifiManager = MainApplication.applicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiInfo = wifiManager.getConnectionInfo()

        return wifiInfo.supplicantState == SupplicantState.COMPLETED
    }
}