package com.verticalclock.verticalclockapp.views

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.verticalclock.verticalclockapp.R

/**
 * Created by prasanthasokkumar on 17.03.18.
 */
class VerticalTemperatureView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyle: Int = 0,
        defStyleRes: Int = 0
) : RelativeLayout(context, attrs, defStyle, defStyleRes)
{
    init
    {
        var view = LayoutInflater.from(context)
                .inflate(R.layout.view_vertical_temperature, this, true)

        attrs?.let {

            val a = context.obtainStyledAttributes(it, R.styleable.custom_attributes)
            if (a.hasValue(R.styleable.custom_attributes_touch_disabled))
            {
                var touchDisabled: Boolean = a.getBoolean(R.styleable.custom_attributes_touch_disabled, false)

                view.findViewById<View>(R.id.numberViewOne)?.isEnabled = !touchDisabled
                view.findViewById<View>(R.id.numberViewTwo)?.isEnabled = !touchDisabled
            }

            a.recycle()
        }
    }

    fun setNumber(numberOne: Int, numberTwo: Int)
    {
        var numberViewOne = findViewById<NumberView>(R.id.numberViewOne)
        numberViewOne?.setNumber(numberOne)

        var numberViewTwo = findViewById<NumberView>(R.id.numberViewTwo)
        numberViewTwo?.setNumber(numberTwo)
    }

    fun setNumberWithoutAnimation(numberOne: Int, numberTwo: Int)
    {
        var numberViewOne = findViewById<NumberView>(R.id.numberViewOne)
        numberViewOne?.setNumberWithoutAnimation(numberOne)

        var numberViewTwo = findViewById<NumberView>(R.id.numberViewTwo)
        numberViewTwo?.setNumberWithoutAnimation(numberTwo)
    }

    fun setTemperature (temperature: String)
    {
        Log.d("VerticalTemperatureView - setTemperature", temperature)
        setNumber(temperature.get(0).toString().toInt(), temperature.get(1).toString().toInt())
    }

    fun setTemperatureWithoutAnimation (temperature: String)
    {
        Log.d("VerticalTemperatureView - setTemperature", temperature)
        setNumberWithoutAnimation(temperature.get(0).toString().toInt(), temperature.get(1).toString().toInt())
    }
}
